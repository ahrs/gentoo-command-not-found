#!/usr/bin/env bats

@test "/usr/sbin/powertop" {
  stdout="$(! ./command-not-found /usr/sbin/powertop)"
[ "$stdout" = "The program 'powertop' is currently not installed. You can install it by typing:
sudo emerge sys-power/powertop" ]
}
