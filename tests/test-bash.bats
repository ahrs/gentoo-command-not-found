#!/usr/bin/env bats

@test "/bin/bash" {
  stdout="$(! ./command-not-found /bin/bash)"
[ "$stdout" = "The program 'bash' is currently not installed. You can install it by typing:
sudo emerge app-shells/bash" ]
}
