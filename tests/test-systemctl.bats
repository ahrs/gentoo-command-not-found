#!/usr/bin/env bats

@test "/usr/bin/systemctl" {
  stdout="$(! ./command-not-found /usr/bin/systemctl)"
[ "$stdout" = "The program 'systemctl' is currently not installed. You can install it by typing:
sudo emerge sys-apps/systemd" ]
}
