#!/usr/bin/env bats

@test "/sbin/zfs" {
  stdout="$(! ./command-not-found /sbin/zfs)"
[ "$stdout" = "The program 'zfs' is currently not installed. You can install it by typing:
sudo emerge sys-fs/zfs" ]
}
